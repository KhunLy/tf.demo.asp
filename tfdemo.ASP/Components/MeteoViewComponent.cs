﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using tfdemo.ASP.Models;

namespace tfdemo.ASP.Components
{
    //[ViewComponent]
    public class MeteoViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage reponse = await client.GetAsync(
                    "https://prevision-meteo.ch/services/json/charleroi");
                if(reponse.IsSuccessStatusCode)
                {
                    string json = await reponse.Content.ReadAsStringAsync();
                    MeteoInfoModel model =
                        JsonConvert.DeserializeObject<MeteoInfoModel>(json);
                    return View(model);
                }
                return View(null);
            }
        }


        //public IActionResult Invoke()
        //{

        //}
    }
}
