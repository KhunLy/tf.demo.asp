﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tfdemo.ASP.Models
{
    public class ProductModel
    {
        public int Id { get; set; }

        [Required]
        //[RegularExpression(@"/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/")]
        public string Nom { get; set; }

        [Required]
        [Range(typeof(decimal), "0", "9999")]
        public decimal Prix { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

    }
}
