﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace tfdemo.ASP.Controllers
{
    public class DefaultController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult PageAvecDonnee()
        {
            //test
            List<string> model = new List<string>
            {
                "Khun", "Thierry", "Mike"
            };
            return View(model);
        }

        public IActionResult AfficherDate()
        {
            DateTime date = DateTime.Now;
            return View(date);
        }
    }
}
