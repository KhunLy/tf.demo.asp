﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using tfdemo.ASP.Models;

namespace tfdemo.ASP.Controllers
{
    public class ProductController : Controller
    {
        private static List<ProductModel> dc
            = new List<ProductModel>
            {
                new ProductModel
                { Id = 1, Nom = "Orval", Prix = 4, Description = "Biere Trappiste" },
                new ProductModel
                { Id = 2, Nom = "Carapills", Prix = 1, Description = "Biere de mauvaise qualité" },
                new ProductModel
                { Id = 3, Nom = "Chimay", Prix = 3, Description = "Biere trappiste" },
            };

        public IActionResult Index()
        {
            return View(dc);
        }

        public IActionResult Details(int id)
        {
            ProductModel model = dc.SingleOrDefault(x => x.Id == id);
            if(model == null)
            {
                return NotFound();
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ProductModel model)
        {
            //validation du model
            //si le model est valide
            if (ModelState.IsValid)
            {
                //insertion dans la db
                int maxId = dc.Max(x => x.Id);
                model.Id = maxId + 1;
                dc.Add(model);
                //afficher un message
                //rediriger autre part
                return RedirectToAction("Index");
            }
            // else
            //ajouter les erreurs
            return View(model);
        }
    }
}
